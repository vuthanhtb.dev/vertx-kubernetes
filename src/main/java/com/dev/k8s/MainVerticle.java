package com.dev.k8s;

import io.vertx.config.ConfigChange;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.Counter;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.SockJSBridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.sstore.ClusteredSessionStore;
import io.vertx.ext.web.sstore.SessionStore;

import java.util.UUID;

public class MainVerticle extends AbstractVerticle {

    private static final String INSTANCE_ID = UUID.randomUUID().toString();
    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);
    private static final String REQUEST_COUNTER = "requestCount";
    private static final String APP_NAME = "appName";

    private final JsonObject currentConfig = config();
    private Counter counter = null;

    @Override
    public void start(Promise<Void> startPromise) {
        this.vertx.fileSystem().exists("./config.json").onSuccess(this::initialConfig);

        // set up a router
        Router router = Router.router(this.vertx);

        // log all rest requests
        router.route().handler(this::logRequest);

        // initialize out session management
        SessionStore clusteredSession = ClusteredSessionStore.create(this.vertx);
        router.route().handler(SessionHandler.create(clusteredSession));

        // add rest endpoints
        router.get("/api/v1/pod-info").handler(this::getPodInfo);
        router.get("/api/v1/health-check").handler(this::healthCheck);

        // set up out eventbus bridge
        router.mountSubRouter("/api/v1/eventbus", this.createSockJSEventBusBridge());

        router.get().handler(this.configStaticHandler());

        // create a http server & attach router to server
        this.vertx.createHttpServer()
                .requestHandler(router)
                .listen(this.currentConfig.getInteger("port", 8080))
                .compose(this::initSharedCounter)
                .compose(this::storeReferenceToClusteredCounter)
                .compose(this::triggerPeriodicEventBusMessages)
                .onSuccess(startPromise::complete)
                .onFailure(startPromise::fail);

    }

    private StaticHandler configStaticHandler() {
        return StaticHandler.create()
                .setIndexPage("index.html")
                .setCachingEnabled(true)
                .setFilesReadOnly(true)
                .setWebRoot(this.currentConfig.getString("webroot", "webroot"))
                .setDirectoryListing(false);
    }

    private Future<Void> triggerPeriodicEventBusMessages(Void unused) {
        this.vertx.setPeriodic(500, this::sendPeriodic);
        return Future.succeededFuture();
    }

    private void sendPeriodic(Long timerId) {
        this.counter.get().onSuccess(this::sendStatusWithRequestCount);
    }

    private void sendStatusWithRequestCount(Long count) {
        JsonObject message =
                new JsonObject()
                        .put("id", INSTANCE_ID)
                        .put(REQUEST_COUNTER, count)
                        .put(APP_NAME, currentConfig.getString(APP_NAME, "VERTX_K8S"));
        vertx.eventBus().send("status", message);
    }

    private Future<Void> storeReferenceToClusteredCounter(Counter counter) {
        this.counter = counter;
        return Future.succeededFuture();
    }

    // The HttpServer object is ignore, but needed for the method signatures to match
    private Future<Counter> initSharedCounter(HttpServer server) {
        return vertx.sharedData().getCounter(REQUEST_COUNTER);
    }

    private Router createSockJSEventBusBridge() {
        final SockJSBridgeOptions sockJSBridgeOptions = new SockJSBridgeOptions()
                .addInboundPermitted(new PermittedOptions().setAddressRegex(".*"))
                .addOutboundPermitted(new PermittedOptions().setAddressRegex(".*"));
        return SockJSHandler.create(this.vertx).bridge(sockJSBridgeOptions);
    }

    private void healthCheck(RoutingContext ctx) {
        ctx.response()
                .setStatusCode(200)
                .setStatusMessage("OK")
                .putHeader("Content-Type", "application/json")
                .end("OK");
    }

    private void getPodInfo(RoutingContext ctx) {
        Session session = ctx.session();
        JsonObject podInfo = new JsonObject();
        podInfo.put("id", INSTANCE_ID);

        Integer requestCount = (Integer) session.data().getOrDefault(REQUEST_COUNTER, 0);
        session.data().put(REQUEST_COUNTER, requestCount + 1);

        ctx.response()
                .setStatusCode(200)
                .setStatusMessage("OK")
                .putHeader("Content-Type", "application/json")
                .end(podInfo.encodePrettily());

        LOGGER.info(String.format("Pod information: %s", podInfo.encodePrettily()));
    }

    private void logRequest(RoutingContext ctx) {
        LOGGER.info(String.format("Request: %s", ctx.request().path()));
        ctx.next();
    }

    private void initialConfig(Boolean hasConfigJson) {
        ConfigRetrieverOptions configRetrieverOptions = new ConfigRetrieverOptions();
        if (hasConfigJson) {
            configRetrieverOptions.addStore(
                    new ConfigStoreOptions()
                            .setType("file")
                            .setFormat("json")
                            .setConfig(new JsonObject().put("path", "./config.json"))
            );
        }

        if (System.getenv().containsKey("KUBERNETES_NAMESPACE")) {
            configRetrieverOptions.addStore(
                    new ConfigStoreOptions()
                            .setType("configmap")
                            .setConfig(
                                    new JsonObject()
                                            .put("namespace", System.getenv().getOrDefault("KUBERNETES_NAMESPACE", "default"))
                                            .put("name", "VERTX_K8S_DEMO")
                            )
            );
        }

        ConfigRetriever.create(this.vertx, configRetrieverOptions).listen(this::loadNewConfig);
    }

    private void loadNewConfig(ConfigChange configChange) {
        this.currentConfig.mergeIn(configChange.getNewConfiguration());
    }
}
